export interface IMovieData {
  [index: string]: string | number;
}