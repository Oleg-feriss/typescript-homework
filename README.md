# MOVIE APP

## Instalation

`npm install`

## Run project

`npm run start`

Open http://localhost:9090/ to view it in the browser.

## Check typescript and linter errors

`npm run lint`

## API Key (v3 auth) - f9d6c9988d45e80865767bb36aa411bb